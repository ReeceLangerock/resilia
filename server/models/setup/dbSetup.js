const { faker } = require("@faker-js/faker");
const mongo = require("../database");

const createFakeData = () => {
  let data = [];
  for (let i = 1; i <= 50; i++) {
    const now = new Date();
    const newData = {
      id: i,
      name: faker.name.findName(),
      email: faker.internet.email(),
      date: now.setDate(now.getDate() + i),
      amount: Math.floor(Math.random() * (100000 - 10 + 1) + 10),
      note: faker.lorem.sentence(),
    };
    data.push(newData);
  }
  return data;
};

async function prefillDB() {
  try {
    await mongo.init();
    console.log("starting prefill");
    const fakeData = createFakeData();
    // await mongo.db.collection("notifications").deleteMany({});
    const result = await mongo.db.collection("notifications").insertMany(fakeData);
    console.log("prefill complete", result);
  } catch (e) {
    console.log(e);
  }
}

prefillDB();
