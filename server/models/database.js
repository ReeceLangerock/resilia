const { MongoClient } = require("mongodb");
require("dotenv").config();

class MongoConnection {
  constructor() {
    const url = process.env.LOCAL_MONGO
      ? `${process.env.MONGO_URI}`
      : `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.1jwvt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

    this.client = new MongoClient(url);
  }
  async init() {
    await this.client.connect();
    console.log("connected to mongodb");
    this.db = this.client.db("test");
    return this.db;
  }
}

module.exports = new MongoConnection();
