const router = require("express").Router();
const { db } = require("../models/database");

router.get("/notifications/:userId", async (req, res) => {
  try {
    if (!req.user || req.params.userId !== req.user.id) {
      req.user = await findOrCreateUser(req.params.userId);
    }
    const newNotifications = await getNewNotifications(req.user.lastSync);
    const viewedNotifications = await getViewedNotifications(req.user.lastSync);

    if (newNotifications.length) {
      const lastNotification = newNotifications[newNotifications.length - 1].id;
      await updateUser(req.user.id, lastNotification);
    }
    res.status(200).json({ newNotifications, viewedNotifications });
  } catch (e) {
    res.status(400).json({ error: "Failed to retreive notifications" });
  }
});

async function findOrCreateUser(userId) {
  let user = await db.collection("users").findOne({ id: userId });
  if (!user) {
    user = { id: userId, lastSync: 0 };
    await db.collection("users").insertOne(user);
  }
  return user;
}

async function updateUser(userId, sync) {
  await db.collection("users").findOneAndUpdate({ id: userId }, { $set: { lastSync: sync } });
}

async function getNewNotifications(lastId) {
  const randomCount = Math.ceil(Math.random() * 3);
  return db
    .collection("notifications")
    .find({ id: { $gt: lastId, $lte: lastId + randomCount } })
    .sort({ id: -1 })
    .limit(randomCount)
    .toArray();
}

async function getViewedNotifications(lastId) {
  return db
    .collection("notifications")
    .find({ id: { $lte: lastId } })
    .sort({ id: -1 })
    .toArray();
}
module.exports = router;
