const express = require("express");
require("dotenv").config();
const app = express();
const PORT = process.env.PORT || 3001;
const mongo = require("./models/database");

async function start() {
  try {
    await mongo.init();
    app.listen(PORT, () => {
      console.log("Server is Listening on Port ", PORT);
    });

    app.use("/resetUsers", async (req, res) => {
      // DELETE ME
      console.log("RESETTING USERS");
      await mongo.db.collection("users").deleteMany({});
      console.log("USERS RESET", await mongo.db.collection("users").find({}).toArray());
    });

    app.use("/api", require("./routes/notifications"));
  } catch (e) {
    console.error("FAILED TO START SERVER");
  }
}
start();
