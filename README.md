# Resilia Coding Exercise

This exercise was build with Create-React-App on the front-end, and some Material UI components. The server is a basic express server with a MongoDB database.

## How to get started.

The simplest way to run the app is by connecting to the MongoDB Atlas database. To do so, you'll first need to create a `.env` file in the server folder. The variables needed were sent via email.

Then you'll need to cd into the client folder. Once there, to `npm install` for both folders you can run: 
- `npm run setup` 

While still in `client`  folder, run client and server with concurrently by running: 
- `npm run watch`

Everything should now be up and running!