import React, { useState, useEffect, useCallback } from "react";
import { Container, Typography, Divider, Paper } from "@mui/material";
import Refresh from "./components/RefreshButton";
import { styled } from "@mui/system";

import NotificationTable from "./components/NotificationTable";
import Notifications from "./components/Notifications";

function App() {
  // mock a fake user, server will use to track viewed notifications
  const [user, setUser] = useState({ id: 123, name: "Reece" });
  const [newNotifications, setNewNotifications] = useState([]);
  const [viewedNotifications, setViewedNotifications] = useState([]);

  const fetchNotifications = useCallback(async () => {
    const response = await fetch(`/api/notifications/${user.id}`);
    if (!response.ok) {
      // should display error component here
      return;
    }
    const data = await response.json();
    if (data) {
      setViewedNotifications(data.viewedNotifications);
      setNewNotifications(data.newNotifications);
    }
  }, [user.id]);

  useEffect(() => {
    fetchNotifications();
  }, [fetchNotifications]);

  return (
    <StyledContainer maxWidth="xl">
      <Paper sx={{ padding: "1rem", marginBottom: "2rem" }}>
        <Row>
          <Image src="https://images.squarespace-cdn.com/content/v1/5e45fd7b05ae4e3f2f2ed60f/1581645908407-OU71JDZN3L0QV38JKDGQ/Resilia_Final_FullColor.jpg" />
          <Refresh handleClick={fetchNotifications} />
        </Row>
        <Notifications notifications={newNotifications} />
      </Paper>
      <NotificationTable notifications={viewedNotifications} />
    </StyledContainer>
  );
}

export default App;

const StyledContainer = styled(Container)({
  padding: 8,
  borderRadius: 4,
  minHeight: "calc(100vh - 2rem)",
  margin: "1rem auto",
});

const Image = styled("img")({
  height: "50px",
});

const Row = styled("div")({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  paddingRight: "1rem",
  margin: "0 0 2rem ",
  backgroundColor: "#fff",
});
