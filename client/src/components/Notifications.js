import { Typography, Card, CardContent, CardHeader } from "@mui/material";
import { styled } from "@mui/system";

export default function Notifications({ notifications = [] }) {
  return (
    <>
      <Typography variant="h3" color="fontSecondary" align="left">
        New Donations
      </Typography>
      <Typography variant="subtitle1" align="left" gutterBottom>
        You have {notifications.length} new notification
        {notifications.length !== 1 ? "s" : ""}.
      </Typography>
      <Row>
        {notifications.length > 0 &&
          notifications.map((note) => <Note notification={note} key={note.id}></Note>)}
      </Row>
    </>
  );
}

function Note({ notification }) {
  const date = new Date(notification.date);
  const formattedDate = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
  const formattedAmount = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    maximumFractionDigits: 0,
  }).format(notification.amount);

  return (
    <Card variant="outlined" aria-label="notification">
      <CardHeader
        title={notification.name}
        subheader={formattedDate}
        sx={{ padding: ".5rem 1rem .25rem" }}
      />
      <CardContent sx={{ padding: ".25rem 1rem .25rem" }}>
        <Typography sx={{ fontSize: 16 }} color="text.primary">
          <b>Donation Amount:</b> {formattedAmount}
        </Typography>
        <Typography sx={{ fontSize: 14 }} color="text.primary">
          <b>Donor Email:</b> {notification.email}
        </Typography>
        <Typography sx={{ fontSize: 14 }} color="text.primary">
          <b>Note from Donor:</b> {notification.note}
        </Typography>
      </CardContent>
    </Card>
  );
}

const Row = styled("div")({
  display: "grid",
  gridGap: ".5rem",
  gridTemplateColumns: "repeat(auto-fill, minmax(200px, 350px))",
  paddingRight: "1rem",
  margin: "1.5rem 0 3rem ",
});
