import { Fab } from "@mui/material";
import { styled } from "@mui/system";

const StyledFab = styled(Fab)(({ theme }) => ({
  backgroundColor: theme.brand.secondary,
  color: theme.font.white,
  fontSize: "1rem",
  "&:hover": {
    backgroundColor: theme.brand.primary,
  },
}));

export default function Refresh({ handleClick }) {
  return (
    <StyledFab variant="extended" onClick={handleClick}>
      Refresh
    </StyledFab>
  );
}
