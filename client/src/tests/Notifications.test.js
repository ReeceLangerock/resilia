import { render, screen, fireEvent } from "@testing-library/react";
import Notifications from "../components/Notifications";

const mockNotifications = [
  {
    amount: 44651,
    date: 1654896524999,
    email: "Otilia11@gmail.com",
    id: 50,
    name: "Joanne Hodkiewicz",
    note: "Quia rerum qui eos eum aliquam debitis.",
  },
  {
    amount: 56486,
    date: 1654810124999,
    email: "Tiana11@hotmail.com",
    id: 49,
    name: "Carlos Langosh",
    note: "Quia rerum qui eos eum aliquam debitis.",
  },
];

test("renders correct title text", () => {
  render(<Notifications notifications={[]} />);
  const title = screen.getByText(/New Donations/i);
  expect(title).toBeInTheDocument();
});

test("renders correct label text", () => {
  render(<Notifications notifications={[]} />);
  const subtitle = screen.getByText(/You have 0 new notifications./i);
  expect(subtitle).toBeInTheDocument();
});

test("renders correct label text - 1 notification", () => {
  render(<Notifications notifications={[{ id: 1 }]} />);
  const subtitle = screen.getByText(/You have 1 new notification./i);
  expect(subtitle).toBeInTheDocument();
});

test("renders notifacations correctly", () => {
  render(<Notifications notifications={mockNotifications} />);
  const notifications = screen.getAllByLabelText(/notification/i);
  expect(notifications.length).toEqual(2);
});

test("renders notifacation content correctly", () => {
  render(<Notifications notifications={mockNotifications} />);
  const notifications = screen.getAllByLabelText(/notification/i);
  expect(notifications[0]).toHaveTextContent("Joanne Hodkiewicz");
  expect(notifications[0]).toHaveTextContent("6/10/2022");
  expect(notifications[0]).toHaveTextContent("Donation Amount: $44,651");
  expect(notifications[0]).toHaveTextContent("Donor Email: Otilia11@gmail.com");
  expect(notifications[0]).toHaveTextContent(
    "Note from Donor: Quia rerum qui eos eum aliquam debitis."
  );
});
