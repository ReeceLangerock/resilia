import { render, screen, fireEvent } from "@testing-library/react";
import RefreshButton from "../components/RefreshButton";
import { ThemeProvider } from "@mui/material/styles";
import theme from "./../theme";

test("renders learn correct button text", () => {
  render(
    <ThemeProvider theme={theme}>
      <RefreshButton />
    </ThemeProvider>
  );
  const buttonText = screen.getByText(/Refresh/i);
  expect(buttonText).toBeInTheDocument();
});

test("calls handleClick on click", () => {
  const handleClick = jest.fn();
  render(
    <ThemeProvider theme={theme}>
      <RefreshButton handleClick={handleClick} />
    </ThemeProvider>
  );
  fireEvent.click(screen.getByText(/Refresh/i));
  expect(handleClick).toHaveBeenCalled();
});
