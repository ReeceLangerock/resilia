import { createTheme } from "@mui/material/styles";

export default createTheme({
  brand: { primary: "#143f7e", secondary: "#42d869", background: "#f3f4f4" },
  font: { white: "#fff" },
  palette: {
    primary: {
      main: "#0052cc",
    },
    secondary: {
      main: "#edf2ff",
    },
  },
});
